#include <stdio.h>
#include "Pool.h"
#include "Contig_pool.h"

int main(int argc, char *argv[]) {

    int *x[4];
    struct Pool pool;
    struct Contig_pool contig_pool;

    // Test pool.
    if (Pool_make(&pool, sizeof(int), 12) != 0) {
        printf("error: Pool_make\n");
        return 1;
    }
    x[0] = Pool_alloc(&pool);
    *x[0] = 0;
    x[1] = Pool_alloc(&pool);
    *x[1] = 1;
    x[2] = Pool_alloc(&pool);
    *x[2] = 2;
    Pool_dealloc(&pool, x[1]);
    x[3] = Pool_alloc(&pool);
    printf(
        "%d, %d, %d, %d\n",
        *x[0],
        *x[1],
        *x[2],
        *x[3]
    );
    Pool_destroy(&pool);

    // Test contig pool.
    if (Contig_pool_make(&contig_pool, sizeof(int), 12) != 0) {
        printf("error: Contig_pool_make\n");
        return 1;
    }
    x[0] = Contig_pool_alloc(&contig_pool);
    *x[0] = 0;
    x[1] = Contig_pool_alloc(&contig_pool);
    *x[1] = 1;
    x[2] = Contig_pool_alloc(&contig_pool);
    *x[2] = 2;
    // Dealloc breaks last allocated pointer.
    Contig_pool_dealloc(&contig_pool, x[1]);
    x[3] = Contig_pool_alloc(&contig_pool);
    printf(
        "%d, %d, %d\n",
        *x[0],
        *x[1],
        *x[2],
        *x[3]
    );
    Contig_pool_destroy(&contig_pool);

    return 0;
}
