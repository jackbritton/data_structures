CC     = gcc
CFLAGS = -Wall
LFLAGS = -lm
OBJS   = main.o Pool.o Contig_pool.o
TARGET = data_structures

all : $(TARGET) run

$(TARGET) : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o $(TARGET)

run : $(TARGET)
	./$(TARGET)

clean :
	rm $(TARGET) $(OBJS)
