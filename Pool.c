#include "Pool.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>

int Pool_make(struct Pool *pool, int cell_size, int max) {
    int occupied_len;
    // Data.
    pool->data = malloc(cell_size*max);
    if (pool->data == NULL)
        return 1;
    // Occupied bitmap.
    occupied_len = ceil((double) max/8);
    pool->occupied = malloc(occupied_len);
    if (pool->occupied == NULL)
        return 1;
    memset(pool->occupied, 0, occupied_len);

    pool->cell_size = cell_size;
    pool->max = max;

    return 0;
}
int Pool_destroy(struct Pool *pool) {
    free(pool->data);
    free(pool->occupied);
    return 0;
}
void *Pool_alloc(struct Pool *pool) {
    // Find an empty spot.
    int iter;
    for (iter = 0; iter < pool->max; iter++) {
        if ((pool->occupied[iter/8] >> (iter%8)) & 1)
            continue;
        // Found an empty spot - set the occupied slot.
        pool->occupied[iter/8] |= (1 << (iter%8));
        return pool->data + iter*pool->cell_size;
    }
    return NULL;
}
int Pool_dealloc(struct Pool *pool, void *ptr) {
    int index;
    // Exit if the pointer isn't within the pool.
    if (ptr < pool->data)
        return 1;
    if (ptr >= pool->data + pool->max*pool->cell_size)
        return 1;
    // Find index so we can clear the occupied slot.
    index = (ptr - pool->data) / pool->cell_size;
    pool->occupied[index/8] &= ~(1 << (index%8));
    ptr = NULL;
    return 0;
}
