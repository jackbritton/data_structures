#ifndef POOL_H
#define POOL_H

struct Pool {
    void *data;
    unsigned char *occupied; // Bitmap of occupied cells.
    int max,
        cell_size;
};
int Pool_make(struct Pool *pool, int cell_size, int max);
int Pool_destroy(struct Pool *pool);
void *Pool_alloc(struct Pool *pool);
int Pool_dealloc(struct Pool *pool, void *ptr);

#endif
